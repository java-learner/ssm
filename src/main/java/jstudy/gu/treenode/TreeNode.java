package jstudy.gu.treenode;

import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TreeNode {

	@TableId(type = IdType.AUTO)
	private Integer id;// mid mkey

	private String name;

	private String content;

	private Integer parentId;

	@TableField(exist = false) // mybatis plus 注解,. 表明此字段在数据库中不存在
	private List<TreeNode> _children;
}
