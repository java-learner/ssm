package jstudy.gu.treenode;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import jstudy.gu.kit.string.StringKit;

@RequestMapping("/treenode")
@RestController
@ResponseBody
@Transactional
public class TreeController { // spring 管理bean 的时候,. name 是默认首字母大写,其他一样
	@Autowired
	private TreeNodeService service;

	@GetMapping("/get/tree")
	public Object getTree() {
		return service.getTree(0);
	}

	@GetMapping("/search/name")
	public Object searchTree(@RequestParam Map<String, Object> search) {
		String name = StringKit.of(search.get("name"));
		if (name.isEmpty())
			return service.getTree(0);
		return service.searchName(name);
	}
	@GetMapping("/search2/name")
	public Object searchTree2(@RequestParam Map<String, Object> search) {
		String name = StringKit.of(search.get("name"));
		if (name.isEmpty())
			return service.getTree(0);
		return service.searchName2(name);
	}
}
