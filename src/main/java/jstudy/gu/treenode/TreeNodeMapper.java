package jstudy.gu.treenode;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface TreeNodeMapper extends BaseMapper<TreeNode> {

	@Select("select * from tree_node where parent_id = #{parent_id}")
	List<TreeNode> selectByParent(int parent_id);

	
	@Select("select * from tree_node where name like '%${name}%'")
	List<TreeNode> selectNameLike(String name);

}
