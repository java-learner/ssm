package jstudy.gu.aop;

import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

	@ResponseBody
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ValidError handleValidException(MethodArgumentNotValidException e, HttpServletResponse resp) {
		// 日志记录错误信息
		// 将错误信息返回给前台
		resp.setStatus(412); // 规定一个http code 专门用来表示@Valid 错误
		FieldError fe = e.getBindingResult().getFieldError();
		ValidError validError = new ValidError(fe.getField(), fe.getDefaultMessage());
		log.error(validError.toString());
		return validError;
	}
}