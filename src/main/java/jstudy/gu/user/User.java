package jstudy.gu.user;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class User {
	@TableId(type = IdType.AUTO)
	private Integer id;
	@NotBlank(message = "{nameNotBlank}")
	private String name;
	
	@NotNull(message = "ageNotNull")
	@Min(value = 1,message = "ageNotBelow1")
	@Max(value = 100,message = "ageNotGt100")
	private Integer age;

}
