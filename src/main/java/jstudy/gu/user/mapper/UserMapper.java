package jstudy.gu.user.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import jstudy.gu.user.User;

@Mapper
public interface UserMapper extends BaseMapper<User>{
	
	@Select("select * from user where age > #{age}")
	List<User> selectByAgeGT(User user);
}
