package jstudy.gu.kit.reflect;

import java.lang.reflect.Field;

import lombok.SneakyThrows;

public final class ReflectKit {

	@SneakyThrows
	public static void setField(Object o, String fieldName, Object value) {
		Class<? extends Object> clz = o.getClass();
		Field f = clz.getDeclaredField(fieldName);
		f.setAccessible(true);
		f.set(o, value);
	}

	@SuppressWarnings("unchecked")
	@SneakyThrows
	public static <T> T getField(Object o, String fieldName) {
		Class<? extends Object> clz = o.getClass();
		Field f = clz.getDeclaredField(fieldName);
		f.setAccessible(true);
		return (T) f.get(o);
	}
}
