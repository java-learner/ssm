package jstudy.gu.kit.string;

import java.sql.Blob;
import java.util.Collection;
import java.util.stream.Stream;

import lombok.SneakyThrows;

public final class StringKit {
	@SneakyThrows
	public static String of(Object o) {
		if (o == null) {
			return "";
		}
		if (o instanceof String || o instanceof CharSequence) {
			return o.toString();
		}
		if (o instanceof Collection<?>) {
			return of((Collection<?>) o);
		}
		if (o instanceof Stream) {
			return of((Stream<?>) o);
		}
		if (o instanceof byte[]) {
			return new String((byte[]) o, "utf-8");
		}
		if (o instanceof Blob) {
			return of((Blob) o);
		}
		return o.toString();
	}

}
