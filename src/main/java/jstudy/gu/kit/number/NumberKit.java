package jstudy.gu.kit.number;

public class NumberKit {
	private NumberKit() {
	}

	public static Integer IntegerOf(Object o) {
		if (o == null) {
			return null;
		}
		if (o instanceof Integer) {
			return (Integer) o;
		}
		if (o instanceof Number) {
			return ((Number) o).intValue();
		}
		return IntegerOf(o.toString());
	}

	public static Integer IntegerOf(String s) {
		if (s == null || (s.isEmpty())) {
			return null;
		}
		return Integer.valueOf(s);
	}
}
