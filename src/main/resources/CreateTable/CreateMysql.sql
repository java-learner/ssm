drop table if exists `tree_node`;
CREATE TABLE `tree_node` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
INSERT INTO tree_node (id,name,content,parent_id) VALUES
	 (1,'1用户需求:商品进货','商品需要进货,将库存保存起来',0),
	 (2,'2用户需求:商品销售','商品需要进货,将商品出售后,减少库存',0),
	 (3,'1.1软件需求:商品进货','商品需要进货,将库存保存起来',1),
	 (4,'2.1软件需求:商品销售','商品需要进货,将商品出售后,减少库存',2),
	 (5,'2.1.1设计:商品销售','商品需要进货,将商品出售后,减少库存',4),
	 (6,'1.1.1设计:商品进货','商品需要进货,将库存保存起来',3),
	 (7,'2.1.2开发:商品销售-未完成','商品需要进货,将商品出售后,减少库存',4),
	 (8,'2.1.3测试:商品销售-未完成','商品需要进货,将商品出售后,减少库存',4),
	 (9,'1.1.2开发:商品进货','商品需要进货,将库存保存起来',3),
	 (10,'1.1.3测试:商品进货','商品需要进货,将库存保存起来',3);
drop table if exists `tree_node`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;