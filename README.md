# ssm
SpringBoot + Mybatis Plus + SpringMvc
## 其他内容
+ hibernate validator(i18n处理)
```
pom中的依赖
ValidError.java
GlobalExceptionHandler.java
```
+ ControllerAspect
```
ControllerAspect.java 切面编程来处理一些公共事情,
比如slowRequest (设置一个阈值,超过阈值的请求接口记录下来使用的时间,以便获得需要优化的接口数据)
比如 统一记录日志 异常 事务等
```

+ 数字处理
```
NumberKit.java
```

+ TreeSearch
```
使用Jdk8 的function 来处理业务中tree结构搜索
```

+ TreeSearch2
```
使用反射来处理TreeSearch,以适应jdk8以下, 或不喜欢/不习惯lambda的情况
```